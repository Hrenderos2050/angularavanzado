import { BrowserModule } from '@angular/platform-browser';
import { NgModule,LOCALE_ID } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from "./header/header.component";
import {FooterComponent } from './footer/footer.component';
import { DirectivaComponent } from "./directiva/directiva.component";
import {ClientesComponent} from './clientes/clientes.component';
import {ClienteService}  from './clientes/clientes.service';
import {ModalService}from './clientes/detalles/modal.service';
//import{FileUpLoadComponent}from './fileupload/fileupload.component';

import {RouterModule,Routes} from "@angular/router";

import{HttpClientModule}from "@angular/common/http";
import { FormComponent } from './clientes/form.component';

import {FormsModule} from "@angular/forms";
import { UploadFileComponent } from './upload-file/upload-file.component';
import {registerLocaleData} from "@angular/common";
import localeES from '@angular/common/locales/es';
import { PaginadorComponent } from './paginador/paginador.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatDatepickerModule,} from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { DetallesComponent } from './clientes/detalles/detalles.component';
import { LoginComponent } from './usuarios/login.component';

registerLocaleData(localeES,'es');

const routes:Routes=[
  {path:'',redirectTo:'/clientes',pathMatch:'full'},
  {path:'directiva',component:DirectivaComponent},
  {path:'clientes',component:ClientesComponent},
  {path:'clientes/form',component:FormComponent},
  {path:'clientes/form/:id',component:FormComponent},
  {path:'clientes/pages/:page',component:ClientesComponent},
  {path:'login',component:LoginComponent},
//  {path:'clientes/ver/:id',component:DetallesComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    DirectivaComponent,
    ClientesComponent,
    FormComponent,
    UploadFileComponent,
    PaginadorComponent,
    DetallesComponent,
    LoginComponent


  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
     MatMomentDateModule
    //FileUploadModule,FileSelectDirective
  ],
  providers: [ClienteService,{provide: LOCALE_ID, useValue: 'es' }, MatDatepickerModule,
     MatMomentDateModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
