import {Injectable} from "@angular/core";
import {DatePipe} from "@angular/common";
import {CLIENTES} from "./cliente.json";
import {Cliente} from "./cliente";
import {Region} from "./region";
import {UpLoadFiles} from "./upLoadFiles";
import {Observable,throwError} from 'rxjs';
import {of} from 'rxjs';
import {HttpClient,HttpHeaders,HttpRequest,HttpEvent} from "@angular/common/http";
import {catchError,map,tap} from 'rxjs/operators';
import swal from 'sweetalert2';
import {Router} from '@angular/router';

@Injectable()
export class ClienteService{
private endPoint:string="http://localhost:8584/api/clientes";
private endPoint2:string="http://localhost:8584/api/cliente";
private httpHeaders=new HttpHeaders({'Content-type':'application/json'})
  constructor(private http :HttpClient,private router:Router){}

getCliente(): Observable <Cliente[]>{
//  return this.http.get<Cliente[]>(this.endPoint);
  return this.http.get(this.endPoint).pipe(
    tap(response=>{
      let clientes =response as Cliente[];
      clientes.forEach(cliente=>{
        console.log(cliente.nombre);
      })
    }),
    map(response => {
      let clientes =response as Cliente[];
      return clientes.map(cliente=>{
        cliente.nombre=cliente.nombre.toUpperCase();
        //cliente.createAt=formatDate(cliente.createAt,'dd-MM-yyyy','en-US');

        let pipe=new DatePipe('es')
      //  cliente.createAt=pipe.transform(cliente.createAt,'fullDate');
        return cliente;
      })
    })
  )
}
getClientePages(pages:number): Observable <any>{
//  return this.http.get<Cliente[]>(this.endPoint);
  return this.http.get(this.endPoint+'/pages/'+pages).pipe(
    tap((response:any)=>{
      (response.content as Cliente[]).forEach(cliente=>{
        console.log(cliente.nombre);
      })
    }),
    map((response:any) => {

       (response.content as Cliente[]).map(cliente=>{
        cliente.nombre=cliente.nombre.toUpperCase();
        //cliente.createAt=formatDate(cliente.createAt,'dd-MM-yyyy','en-US');

        //let pipe=new DatePipe('es')
      //  cliente.createAt=pipe.transform(cliente.createAt,'fullDate');
        return cliente;
      });
      return response;
    })
  )
}


/*  getCliente(): Observable <Cliente[]>{
    return of (CLIENTES);
  }*/

create(cliente:Cliente):Observable<Cliente>{
  return this.http.post<Cliente>(this.endPoint,cliente,{headers:this.httpHeaders}).pipe(
    map((response:any)=> response.cliente as Cliente),
    catchError(e=>{

      if(this.isNotAutorizado(e)){
          return throwError(e);
      }
      if(e.status==400){
        return throwError(e);
      }
      swal.fire('Error al crear',e.error.mensaje,'error');
        return throwError(e);
    }

    )
  )
}

buscarCliente(id):Observable<Cliente>{
  return this.http.get<Cliente>(`${this.endPoint2}/${id}`).pipe(
    catchError(e =>{
      if(this.isNotAutorizado(e)){
          return throwError(e);
      }
      this.router.navigate(['/clientes']);
     swal.fire('Error al editar',e.error.mensaje,'error');
      return throwError(e);
    }

    )
  );
}

actualizarcliente(cliente:Cliente):Observable<any>{

  return this.http.put<any>(`${this.endPoint2}/${cliente.id}`,cliente,{headers:this.httpHeaders}).pipe(

    catchError(e=>{
      if(this.isNotAutorizado(e)){
          return throwError(e);
      }
      if(e.status==400){
        return throwError(e);
      }
      swal.fire('Error al crear',e.error.mensaje,'error');
        return throwError(e);
    }
  ))
}

eliminarCliente(id:number){

  return this.http.delete(`${this.endPoint2}/${id}`,{headers:this.httpHeaders}).pipe(
    catchError(e=>{
      if(this.isNotAutorizado(e)){
          return throwError(e);
      }
      swal.fire('Error al crear',e.error.mensaje,'error');
        return throwError(e);
    }
  ))
}

uploadFile(upload:UpLoadFiles):Observable<HttpEvent<{}>>{
  const req = new HttpRequest('POST', `${this.endPoint2}/upload`,upload, {
    headers:this.httpHeaders,
    reportProgress: true
  });
  return this.http.request(req).pipe(
    catchError(e=>{
      if(this.isNotAutorizado(e)){
          return throwError(e);
      }
    })
  );
}

getRegiones():Observable<Region[]>{
  return this.http.get<Region[]>(`${this.endPoint2}/regiones`).pipe(
    catchError(e=>{
      this.isNotAutorizado(e);
      return throwError(e);
    })
  );
}

private isNotAutorizado(e):boolean{
  if(e.status==401||e.status==403){
    this.router.navigate(['login'])
    return true;
  }
  return false;
}

}
