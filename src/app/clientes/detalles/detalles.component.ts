import { Component, OnInit,Input,OnChanges } from '@angular/core';
import{Cliente} from "../cliente";
import{ClienteService}from "../clientes.service";
import{ModalService} from"./modal.service";
import {Router,ActivatedRoute} from "@angular/router";
import {UpLoadFiles} from "../upLoadFiles";
import {HttpEventType}from '@angular/common/http';
import swal from "sweetalert2";
@Component({
  selector: 'detalles-cliente',
  templateUrl: './detalles.component.html',
  styleUrls: ['./detalles.component.css']
})
export class DetallesComponent implements OnInit {
  @Input()client:Cliente;
titulo:string="Detalle cliente";
private imagen:File;
binaryData :ArrayBuffer;
up:UpLoadFiles=new UpLoadFiles;
proceso:number=0;


  constructor(private ClienteService:ClienteService,
 public modalService:ModalService) {
  }

  ngOnInit(): void {
      //this.modealSer=this.modallService;
    //console.log('OnChangesLocal',this.modealSer.modalDialog);


  }
  seleccionarArchivo(event:any){
         this.imagen = event.target.files[0];
         this.proceso=0;
         if(this.imagen.type.indexOf('image')<0){
             swal.fire('Error al seleccionar imagen','El archivo debe ser de tipo imagen','warning');
             this.imagen=null;
         }else{
          var reader = new FileReader();
          this.up.nombreImagen=this.imagen.name;
          reader.onload = this._handleReaderLoaded.bind(this);
          reader.readAsBinaryString(this.imagen);
  }
}

  _handleReaderLoaded(readerEvt) {
      var binaryString = readerEvt.target.result;
      this.up.imagen = btoa(binaryString);  // Converting binary string data.
  }

subirfoto(){
this.up.id=this.client.id;


if(!this.up.imagen){
    swal.fire('Error Upload','Error debe seleccionar un foto','warning');
}else{
  this.ClienteService.uploadFile(this.up).subscribe(
    event=>{
      if(event.type === HttpEventType.UploadProgress){
        this.proceso=Math.round((event.loaded/event.total)*100);
      }else if(event.type===HttpEventType.Response){
      let response:any=event.body;

      this.client=response.cliente as Cliente;
      console.log(this.client);
      this.modalService.notificarUpload.emit(this.client);
      swal.fire('Archivo Upload',response.mensaje,'success');
      }
      //this.client=cliente;


    }
  )
}
}


cerrarModal(){
this.modalService.cerrarModal();
this.imagen=null;
this.proceso=0;
//console.log('cerrarModal'+this.modallService.modalDialog);
}




}
