import { Component, OnInit } from '@angular/core';
import {Cliente}from "./cliente";
import {Region} from "./region"
import {ClienteService}  from "./clientes.service";
import {Router,ActivatedRoute} from "@angular/router";
import swal from "sweetalert2";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
   cliente: Cliente=new Cliente;
    titulo:string="Crear Cliente"
    errores:string[];
    regiones:Region[];
  constructor(private ClienteService:ClienteService
  ,private router:Router,private activatedRoute:ActivatedRoute) { }

  ngOnInit(): void {
    this.cargarCliente()
  }

  public create():void{
    console.log(this.cliente);
    this.ClienteService.create(this.cliente).
    subscribe(cliente=>{
      this.router.navigate(['/clientes'])
    swal.fire('Nuevo cliente',` Cliente ${cliente.nombre} a sido creado con exito`,'success')
  },err=>{
    this.errores=err.error.errors as string[];
    console.error('codigo del error desde el backend '+err.status);
    console.error(err.error.errors);
  }
    )
    console.log("sumbmit hecho");
      console.log(this.cliente);
  }

  cargarCliente():void{
    this.activatedRoute.params.subscribe(params=>{
      let id =params['id']
      if(id){
        this.ClienteService.buscarCliente(id).subscribe(
          (cliente)=>this.cliente=cliente
        )
      }
    }
   )
   this.ClienteService.getRegiones().subscribe(regiones=>{
     this.regiones=regiones;
   })
  }
  actualizar():void{

    console.log(this.cliente);
    this.ClienteService.actualizarcliente(this.cliente).subscribe(
      json=>{
        this.router.navigate(['/clientes'])
        swal.fire('Cliente Actualizado',`${json.mensaje} : cliente ${json.cliente.nombre}`,'success');
      },err=>{
        this.errores=err.error.errors as string[];
        console.error('codigo del error desde el backend '+err.status);
        console.error(err.error.errors);
      }
    )
  }

  compararRegion(objecto:Region,objecto2:Region){
if(objecto === undefined && objecto2 === undefined){
  return true;
}
  return objecto === null || objecto2 === null ||objecto === undefined || objecto2 === undefined ? false :objecto.nombre===objecto2.nombre;
  }

}
