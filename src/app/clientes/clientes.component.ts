import {Component, OnInit } from "@angular/core";
import {ClienteService}  from "./clientes.service";
import{ModalService} from"./detalles/modal.service";
import {Cliente} from "./cliente";
import swal from "sweetalert2";
import {ActivatedRoute}from '@angular/router';

@Component({
selector:'app-cliente',
templateUrl:'./clientes.component.html'
})

export class ClientesComponent{
 clientes: Cliente [];
 paginador:any;
 ClienteSeleccionado:Cliente;


constructor(
  private clienteService:ClienteService,
  private activatedRoute:ActivatedRoute,
  private modalService:ModalService){}

ngOnInit(){
  //this.clienteService.getCliente().subscribe(

  this.activatedRoute.params.subscribe(params=>{
     let page:number=+params['page']
     if(!page){
       page=0;
     }

  this.clienteService.getClientePages(page).subscribe(
    response=>{
      this.clientes=response.content as Cliente[];
      this.paginador=response;
    }
  /*function(clientes){
      this.clientes=clientes;
    }*/
  );
});
this.modalService.notificarUpload.subscribe(cliente=>{
  this.clientes = this.clientes.map(clienteOriginal=>{
    if(cliente.id == clienteOriginal.id){
      clienteOriginal.foto=cliente.foto;
    }
    return clienteOriginal;
  })
})
}

eliminar(cliente:Cliente):void{

  const swalWithBootstrapButtons = swal.mixin({
  customClass: {
    confirmButton: 'btn btn-success',
    cancelButton: 'btn btn-danger'
  },
  buttonsStyling: false
})

swalWithBootstrapButtons.fire({
  title: 'Esta seguro ?',
  text: `¿Seguro que desea eliminar el cliente ${cliente.nombre} ${cliente.apellidos}?`,
  icon: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Si, eliminar!',
  cancelButtonText: 'No, cancelar!',
  reverseButtons: true
}).then((result) => {
  if (result.value) {
  this.clienteService.eliminarCliente(cliente.id).subscribe(
    response=>{
      this.clientes=this.clientes.filter(cli=>cli!==cliente)
      swalWithBootstrapButtons.fire(
         'Cliente Eliminado !',
        `Cliente ${cliente.nombre} ${cliente.apellidos} eliminado con exito`,
        'success'
      );
    }
  )
  }
})
}
abrirModal(cliente:Cliente){

  this.ClienteSeleccionado=cliente;
 this.modalService.abrirModal();
 console.log("abrir modal");
}

}
