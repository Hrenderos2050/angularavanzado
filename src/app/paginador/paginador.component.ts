import { Component, OnInit,Input ,OnChanges} from '@angular/core';

@Component({
  selector: 'paginador-nav',
  templateUrl: './paginador.component.html',
  styleUrls: ['./paginador.component.css']
})
export class PaginadorComponent implements OnInit,OnChanges {
@Input() paginador:any;
paginas:number[];
desde: number;
hasta: number;
  constructor() { }

  ngOnChanges(): void {

    this.desde = Math.min(Math.max(1,this.paginador.number-4),this.paginador.totalPages - 5);
    this.hasta = Math.max(Math.min(this.paginador.totalPages,this.paginador.number + 4),6);
    if(this.paginador.totalPages>5){
         console.log(this.hasta);
        console.log(this.desde);
    this.paginas=new Array(this.hasta - this.desde + 1).fill(0).map((_valor,indice)=>indice + this.desde);
     console.log(this.paginas);
    }else{

        this.paginas=new Array(this.paginador.totalPages).fill(0).map((_valor,indice)=>indice+1);

      }

  }

  ngOnInit(): void {


}
}
