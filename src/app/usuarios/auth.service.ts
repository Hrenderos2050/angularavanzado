import { Injectable } from '@angular/core';
import{Observable} from 'rxjs';
import{HttpClient,HttpHeaders} from '@angular/common/http';
import{Usuario} from './usuarios';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  private _usuario:Usuario;
  private _token:string;


  constructor(private http:HttpClient) { }

  login(usuario:Usuario):Observable<any>{
     const URL="http://localhost:8584/oauth/token";

     const creadenciales=btoa('angularapp'+':'+'123456');

     const httpheaders=new HttpHeaders({'Content-Type':'application/x-www-form-urlencoded',
     'Authorization':'Basic '+creadenciales});

     let params=new URLSearchParams();
     params.set('grant_type','password');
     params.set('username',usuario.username);
     params.set('password',usuario.password);
     console.log(params.toString());

     console.log(httpheaders);
    return this.http.post(URL,params.toString(),{headers:httpheaders});
  }

  guardarusuario(access_token: string) {

    let peyload=this.obtenerDatos(access_token);

    this._usuario=new Usuario();
    this._usuario.nombre=peyload.nombre;
    this._usuario.apellido=peyload.apellido;
    this._usuario.username=peyload.user_name;
    this._usuario.email=peyload.email
    this._usuario.roles=peyload.authorities,
console.log(this._usuario)
    sessionStorage.setItem('usuario',JSON.stringify(this._usuario));

  }

  guardarToken(access_token: string) {
    this._token=access_token;
    sessionStorage.setItem('token',access_token);
  }

  obtenerDatos(access_token:string):any{
    if(access_token!=null){
      return JSON.parse(atob(access_token.split(".")[1]));
    }
    return null;
  }
  public get usuario():Usuario{
    if(this._usuario!=null){
    return this._usuario
    }else if(this._usuario == null && sessionStorage.getItem('usuario')!=null){
    this._usuario=JSON.parse(sessionStorage.getItem('usuario'))as Usuario;
    console.log(this._usuario)
    return this._usuario
  }else{
    return new Usuario;
  }
  }
  public get token():string{
    if(this._token!=null){
    return this._token
  }else if(this._token==null && sessionStorage.getItem('token')!=null){
    this._token=  sessionStorage.getItem('token');
    return this._token
  }else{
    return null;
  }
  }
  isAutenticated():boolean{
    let payload=this.obtenerDatos(this.token);
    if(payload!=null && payload.user_name && payload.user_name.length>0){
      return true;
    }
    return false;
  }

}
