import { Component, OnInit } from '@angular/core';
import {Usuario} from './usuarios';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';
import swal from 'sweetalert2';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

titulo:string='Por favor Sign In!';
usuario:Usuario;
  constructor(private authservice:AuthService,private router:Router) {
  this.usuario=new Usuario;
 }

  ngOnInit(): void {
    if(this.authservice.isAutenticated()){
       swal.fire('Login','ya esta autenticado','info');
      this.router.navigate(['/clientes'])
    }
  }
 login():void{
   console.log(this.usuario)
   if(this.usuario.username==null||this.usuario.password==null){
     swal.fire('Error login','username o password vacias!','error');
     return;
   }
   this.authservice.login(this.usuario).subscribe(
     reponse=>{
       let payload=JSON.parse(atob(reponse.access_token.split(".")[1]));
       console.log(reponse);
       console.log(payload);

        this.authservice.guardarusuario(reponse.access_token);
        this.authservice.guardarToken(reponse.access_token);
        let usuario=this.authservice.usuario;

             console.log(usuario);
       this.router.navigate(['/clientes']);

       swal.fire('Login','Hola !'+usuario.username+' has iniciado session con exito!','success');
     },error=>{
       if(error.status==400){
         swal.fire('Error login','Username o password Incorrectas!','error');
       }
     }
   );
 }
}
